library(Hmisc)
library("textcat")

### LECTURE DES DONNEES ###

#Modifier le chemin des données suivant

data <- read.csv("/home/fernandes/Documents/Projet_méthodo/EXPORT_2020_10_05.csv", sep=";", quote='\"', dec=",")

var <- data.frame(data$Sleep.Disorder...Other)

### TAILLE DE LA VARIABLE ###
summary(var)
nrow(var)
#longueur de la variable : 9633

### DESCRIPTION DE LA VARIABLE ###

describe(var)

#390 réponses sur 9633 => 9243 missing
#104 réponses distinctes 
#réponse la plus récurrente : UARS VS moins réccurente : adipostas

### langues de la variable ###
lang<-var
for (i in length(var)){
  lang[i]<-(textcat(var[,i]))
}

describe(lang)

#majorité de slovénien, swahili et swedish//minorité bosniaque, catalan, danois


### Garder les réponses écrites ###

var <- data$Sleep.Disorder...Other[data$Sleep.Disorder...Other != ""]
var <- toupper(var)
var <- as.factor(var)
levels(var)

#Nous pouvons prendre les 96 niveaux et trouver des patterns de regroupement

treat <- as.factor(unlist(strsplit(as.character(var), ",")))
treat <- as.factor(unlist(strsplit(as.character(treat), ";")))
treat <- as.factor(gsub("[[:punct:]]","", treat))
treat <- as.factor(gsub("^\\s+|\\s+$", "", treat))
treat <- as.factor(gsub("^\\s+|\\s+$", "", treat))

#après traitement, nous avons 83 levels
unique(treat)

treat2<-treat

#J'utilise une entrée existante comme nom de classe pour cette partie

sleep_disorder <-grep("(RBD|SOMNO|HYPOPNEA|EDS|ATTACKS|SOMNI|SLEEPWALK|DELAYED|OSTEOP|UPPP|SLEEPPHASE|RERAS|WEIGHT|UPP|UARS|SLEEPNESS|NASALSTENOSE|NYKTURIA|MICROSLEEP|DRY THROAT)",treat)
treat2[sleep_disorder]<-as.factor("EDS")

rls <-grep("(PLMD|RESTLESS|RLS|LIMB|PLM|PLMS)",treat)
treat2[rls]<-as.factor("RLS")

snoring <-grep("(SNOR|SNOOR|SNORR|SNORK|NREMP|NOSEBRE|NYCTU|RHONCH|APNOE|RONCHO|CONCHAE|HYPOXEMI)",treat)
treat2[snoring]<-as.factor("SNORING")


sleep_apnea<-grep("(SLEEP APNEA|OBSTRUC|CATATH|APNOE|UARS|UAR|AIRWAY|OSA)",treat)
treat2[sleep_apnea]<-as.factor("MIXED SLEEP APNEA")

sleep_breathing_disorder<-grep("(BREATHING|STETOR|COUGH|RESPIRATION|CHEYNE)",treat)
treat2[sleep_breathing_disorder]<-as.factor("PERIODIC BREATHING")

obesity<-grep("(ADIPOS|OBESI)",treat)
treat2[obesity]<-as.factor("ADIPOSITAS")

#Je transforme les facteurs en chaine de caractères afin de les comparer et d'entrer le vrai nom de la classe
treat2<-as.character(treat2)


for (i in 1:length(treat2)){
  if (treat2[i]== "EDS"){
    treat2[i]<-as.character("SLEEP_DISORDER")}
  else if (treat2[i]== "MIXED SLEEP APNEA"){
    treat2[i]<- "SLEEP_APNEA"}
  else if (treat2[i]== "PERIODIC BREATHING"){
    treat2[i]<- as.character("SLEEP_BREATHING_DISORDER")}
  else if (treat2[i]== "SNORING"){
    treat2[i]<- as.character("SNORING")}
  else if (treat2[i]== "RLS"){
    treat2[i]<- as.character("RLS")}
  else if (treat2[i]== "ADIPOSITAS"){
    treat2[i]<- as.character("OBESITY")}
  else if (is.na(treat2[i])==FALSE){
    treat2[i] <- as.character("OTHERS")}
}

#Nous transformons la variable en facteur pour voir les classes 

treat2<-as.factor(treat2)

#Effecif de chaque classe
summary(treat2)
