library(Hmisc)
library("textcat")

### LECTURE DES DONNEES ###

#Modifier le chemin suivant menant aux données

data <- read.csv("/home/fernandes/Documents/Projet_méthodo/EXPORT_2020_10_05.csv", sep=";", quote='\"', dec=",")


var <- data.frame(data$Other)

### TAILLE DE LA VARIABLE ###
summary(var)
nrow(var)
#longueur de la variable : 9633

### DESCRIPTION DE LA VARIABLE ###

describe(var)

#973 réponses distinctes. 1781 réponses. 7852 missing.
#réponse la plus récurrente : whiplash VS moins réccurente : herniated

### langues de la variable ###
lang<-var
for (i in length(var)){
  lang[i]<-(textcat(var[,i]))
}

describe(lang)

#majorité de espagnol,slovénien, swahili et swedish//minorité bosniaque, afrikaans, basque


### Garder les réponses écrites ###

var <- data$Other[data$Other != ""]
var <- toupper(var)
var <- as.factor(var)
levels(var)

#Nous pouvons prendre les 901 niveaux et trouver des patterns de regroupement

treat <- as.factor(unlist(strsplit(as.character(var), ",")))
treat <- as.factor(unlist(strsplit(as.character(treat), ";")))
treat <- as.factor(gsub("[[:punct:]]","", treat))
treat <- as.factor(gsub("^\\s+|\\s+$", "", treat))
treat <- as.factor(gsub("^\\s+|\\s+$", "", treat))

unique(treat)
#Réduction à 854 levels

treat2<-treat

obesity<- grep("(OBES|OBEZ|LIPO|ADIPOS|HIGH BODY MASS|INCREASED BMI|OVERVEK|OVERWEI|METABO|ABDOM|FAT|PICKWICK|SYKELIG)",treat)
treat2[obesity] <- as.factor("OBESITY")

thyroide<-grep("(THYR|TYR|THIR|THI|TIR|THR|STRUMA|GOIT|GAOT|GOITER|GUATR|RADIOAC|HASIMO|HASSIMO|HASHI|STRUMA|IPOT|GRAVESS)",treat)
treat2[thyroide]<- as.factor("THYROID")

diabetic<-grep("(DIABE|IFG|IGT|INSULIN RESISTANCE)",treat)
treat2[diabetic]<- as.factor("DIABETIC RETINOPATHY")

hepatitis <- grep("(HEPATI|CIRRHOSIS|CYRRHOSE|STEATOSIS|HCV|HEPATO|CEFALEA|LIVER|NASH|NON ALKOHOL FATTY LIVER|NAFLD)",treat)
treat2[hepatitis]<-as.factor("HEP C")

deficiency <-grep("(ANAEMIA|INSUFFIC|ANEMIA|DEFICIENCY|HYPOPHYS)",treat)
treat2[deficiency]<-as.factor("DEFICIENCY FE")

gastric_disorder <-grep("(ERNIA|REFLUX|BLADDER|GASTRIC|GASTRO|GASTRI|GASTROESO|GERD|GEREFLUX)",treat)
treat2[gastric_disorder]<-as.factor("GASTRIC OPERATION")

renal_disorder <-grep("(KIDNEY|RENAL|DIALYSE|NEPHRO|NYRESTEIN|NYRESTEN|NYRESVIKT|PROTEINURIA|ALBUMINURIA|UROLITHIASIS)",treat)
treat2[renal_disorder]<-as.factor("RENAL DISEASE")

blood_disorder <-grep("(HYOGONA|ACROMEGAL|CLL|CONNS|CUSHING|NELSON|HEMOCHRO|HYPERBILI|HYPERCREATI|HYPERGLYCEM|HYPERPROLA|HYPERURIC|HYPOGONA|HYPOKAL|HYPOLIPID|PANHYPOPITU|PANPITUI|HAEMOCHRO|HYPOPITUI)",treat)
treat2[blood_disorder]<-as.factor("HAEMACHROMATOSIS")

prostatic_disorder <-grep("(PROSTAT)",treat)
treat2[prostatic_disorder]<-as.factor("PROSTATE")

tumor <-grep("(ADENOM|TUMOUR|SURRENECTOMY|MACROPROLA|MICROPROLA|TUMOUR)",treat)
treat2[tumor]<-as.factor("HYPOPHYSIS ADENOMA")

cerebral_disease <-grep("(ANEURY|BRAIN)",treat)
treat2[cerebral_disease]<-as.factor("BRAIN ANEURYSM")

heart_disease <- grep("(CARDIA|CVI|PALPITATION|ARRHYTH)",treat)
treat2[cerebral_disease]<-as.factor("CVI")

vascular_disease <- grep("(VENOUS|THROMBO|POLYCY|RAYNAUD|PERIPHE|OEDEMA|VARICOC|TROMBO|STENOS)",treat)
treat2[vascular_disease]<-as.factor("VENOUS THROMBOSIS")

rhinite<-grep("RHINI",treat)
treat2[rhinite]<-as.factor("RHINITIS")

allergy<-grep("ALLERG",treat)
treat2[allergy]<-as.factor("ALLERGI")

sleep_disorder <-grep("(RBD|SOMNO|HYPOPNEA|EDS|ATTACKS|SOMNI|SLEEPWALK|DELAYED|OSTEOP|UPPP|SLEEPPHASE|RERAS|WEIGHT|UPP|UARS|SLEEPNESS|NASALSTENOSE|NYKTURIA|MICROSLEEP|DRY THROAT)",treat)
treat2[sleep_disorder]<-as.factor("INSOMNIA")

rls <-grep("(PLMD|RESTLESS|RLS|LIMB|PLM|PLMS)",treat)
treat2[rls]<-as.factor("RESTLESS LEGS")

tonsillectomy<-grep("(TONIS|TONSI|TOSI|TONSS)",treat)
treat2[tonsillectomy]<-as.factor("TONISLLECTOMY")

kidney_disease<-grep("KIDNEY",treat)
treat2[kidney_disease]<-as.factor("KIDNEY FAILURE")

nasal_obstruction<-grep("OBSTRU",treat)
treat2[nasal_obstruction]<-as.factor("NASAL OBSTRUCTION")

polypectomy<-grep("POLYPE",treat)
treat2[polypectomy]<-as.factor("POLYPECTOMY")

treat2 <-as.character(treat2)
treat2<- as.factor(treat2)


summary(treat2)

#Les classes THYROID, ALLERGI et DEPRESSION ont le plus d'occurrences
#Elles permettent de regrouper un peu moins d'un tiers des réponses en 3 classes principales